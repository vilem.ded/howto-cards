---
card_order: 200
layout: page
permalink: /external/general/getToLCSB/
shortcut: general:getToLCSB
redirect_from:
  - /cards/general:getToLCSB
  - /external/cards/general:getToLCSB
  - /general/getToLCSB
  - /external/external/general/getToLCSB/
---
# How to get to the Luxembourg Centre for Systems Biomedicine

![](img/LCSB.jpg)


### 1. Arriving by plane at Luxembourg airport:
The Luxembourg airport, called Findel, is 34 km away from the University Campus BELVAL where LCSB is located.
You can take either taxi or public transport to reach LCSB.
- **Via Taxi (110€ one way)**

*Price: ca 87-110€ one way (in Luxembourg taxis are very expensive!)*<br>
*Duration: 30 minutes (during rush hour 45-60 min)*<br>
Trustworthy taxi services: [webtaxi](https://www.webtaxi.lu) (tel: +352 27 515); [coluxtaxi](http://colux.lu) (tel: +352 48 22 33). Please ask confirmation of the price.<br>
A map of the Campus BELVAL and how to find both LCSB buildings is shown below.

- **Via public transport (free)**

*Public transportation is free in Luxembourg!*<br>
*Duration: 60 minutes (including bus, train and walk)*<br>

From Luxembourg airport you can take the bus to Luxembourg central train station using bus line 29 and stop at "Central station (Gare Centrale)". For the timetable check the following [link](https://web.vdl.lu/autobus/data/depliants/arrets/AERO903_29.pdf).<br>

From Luxembourg’s central train station, you will get a train every 20 minutes to BELVAL-Université where LCSB is located. Please see below "Arriving by train/bus" for all details.

### 2. Arriving by train/bus at Luxembourg central station:

*Public transportation is free in Luxembourg!*<br>
*Duration: 35 minutes (including train and walk)*<br>

Take the train in direction of "Rodange" or "Petange". Your stop is called: "Belval-Université" station.<br>
A map of the Campus BELVAL and how to find both LCSB buildings is shown below. From Belval train station it's about a 5 minutes-walk to either of our locations.

You can also arrive by bus from Luxembourg City. Please visit [www.mobiliteit.lu](https://www.mobiliteit.lu/en/journey-planner/) to select your travelling options. *Please note, however, that the train is the preferred option.*

### 3. Arriving by car:

*Duration: about 20 minutes from Luxembourg City or 30 minutes from Luxembourg Airport (without traffic jam).*

If you travel by car, please take the highway A4 in direction Esch-sur-Alzette, and stay on that road until it ends at the roundabout called "Raemerich". Do not leave the A4 too early! Simply follow the A4 until it ends, then follow the signs to "Belval" at the large roundabout.<br>
On the campus, unfortunately, there are no signs leading the way to the LCSB. Please use the following plan to orientate on the campus.<br>
The House of Biomedicine (LCSB I) is directly next to the old blast furnace site, close to the *red* Dexia buildings. The building is a bit hidden. It is a six floor modern white building.<br>
Biotech II (LCSB II) is right next to ADEM (Agence pour le développement de l'emploi / Employment Development Agency) and across the street from Southlane Towers.

##### Parking at Campus BELVAL
Since Campus Belval is foreseen to be a green car-free Campus, parking may not be easy. Due to the constructions around
our buildings, we currently cannot offer visitor parking. To park your car we recommend using one of the following. From
either site it is less than five minute walk to either of our buildings.
- Belval Plaza has 2 parking garages<br>
Price: first 3 hours free, afterwards 3€/hour (from 07:00 to 20:00) and 1€/hour (from 20:00 to 07:00)
- Belval-Gare [Park&Rail] parking garage<br>
Price: 1€/hour (from 06:00 to 19:00) and 0.6€/hour (from 19:00 to 06:00)
- ALDI parking (small parking, convinient for short visit of BT2 and Rouden Eck)<br>
Price: first 90 min free, next 1 hour 2€, afterwards 3€/hour

![](img/map.jpg)
